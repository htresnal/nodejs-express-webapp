var proxy = require('express-http-proxy');
var express = require('express');
var app = module.exports = express();

app.set('port', process.env.PORT | 8181);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/dist'));

const apiProxy = proxy('http://www.aisa.ru/', {
  proxyReqPathResolver: function(req) {
    return 'http://www.aisa.ru/images/round_logo@2x.png';
 }
});

app.use(['/logo.png', '/images/round_logo@2x.png'], apiProxy);

app.use(express.static(__dirname + '/views/'));

app.get('/index', function(request, response) {
  response.redirect('/');
});

app.get('/', function(request, response) {
  response.render('./dist/index.html');
});

app.get('/data/date.json', function(request, response){
  response.render('./controllers/getDate.ejs')
});

app.listen(app.get('port'), function() {
  console.log('Node app is up and running on port '+app.get('port'));
});


