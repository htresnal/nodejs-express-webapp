var gulp = require('gulp');
var uglify = require('gulp-uglify');
var browserify = require('gulp-browserify');
var rename = require("gulp-rename");
var fs = require('fs');
const { spawn } = require('child_process');
var livereload = require('gulp-livereload');
var server;

var watchList = ['./**/*', '!./git', '!./node_modules/**/*', '!./dist/*', '!*.tmp*', '!./src/tmp/*', '!./src/public/js/main.js', '!./src/public/js/main.min.js', '!./src/html/index.html'];

gulp.task('default', function() {
    gulp.run('start');
})

gulp.task('start', function() {
    gulp.run('clean', 'restore_dirstructure', 'compress', 'build', 'start_server', 'watch');
})

gulp.task('clean', function() {
    console.log('Cleaning temporary files...\n');

    deleteFileR("./dist/");
    deleteFileR("./src/tmp/");
})

gulp.task('restore_dirstructure', ['clean'], function() {
    console.log('Restoring folder structure...\n');
    
    fs.mkdirSync('./dist');
    fs.mkdirSync('./dist/js');
    return fs.mkdirSync('./src/tmp');
})

gulp.task('compress', ['restore_dirstructure'], function() {
    console.log("Compressing data...\n");
	return gulp.src('./src/clientsideRequireList.js')
        .pipe(browserify({ transform: ['browserify-css'] }))
        .pipe(uglify({mangle: true, compress: true}))
        .pipe(rename("main.min.js"))
	.pipe(gulp.dest('./src/tmp'));
})

gulp.task('build', ['restore_dirstructure' , 'compress'], function() {
    console.log('Building...\n');    
    var randomName=generateRandomName();

    var file = fs.createReadStream('./src/template/index.html', 'utf8');
    var _data = '';
    file.on('data', function (chunk) {
        _data += chunk.toString().replace('/js/main.min.js', '/js/'+randomName+'.js');
    });
    file.on('end', function () {
        fs.writeFileSync('./src/html/index.html', _data);
    });

    fs.rename('./src/tmp/main.min.js', './dist/js/'+randomName+'.js', function (err) { if (err) {throw err}});
    
    return gulp.src(['./src/public/**/*', './src/html/**/*'])
    .pipe( gulp.dest('./dist') );
})

gulp.task('watch', ['build'], function(){
    console.log("Watching...");

    return gulp.watch( watchList , {readDelay: 700, events: 'change'}, function(file){
	console.log("[TRIGGER] Changes detected. Restarting the server.");
        gulp.run(['clean', 'restore_dirstructure', 'compress', 'build', 'restart_server']);
     });
})

gulp.task('start_server', ['build'], function(){
    console.log("Starting a server...");
    server = spawn('node', ['server.js']);
  
    server.on('close', function (code, signal) {
        console.log('child process exited with ' +
	   'code ${code} and signal ${signal}');
    });

    server.stderr.on('data', (data) => {
      console.log(`${data}`);
    });
  
    server.stdout.on('data', (data) => {
      console.log(`${data}`);
    });
  
    livereload.listen({reloadPage: true});
})

gulp.task('restart_server', ['build'], function(){
    console.log("Restarting a server...");
    server.kill('SIGTERM');
  
    server = spawn('node', ['server.js']);

    server.on('close', function (code, signal) {
        console.log('child process exited with ' +
       'code ${code} and signal ${signal}');
    });

    server.stderr.on('data', (data) => {
      console.log(`${data}`);
    });

    server.stdout.on('data', (data) => {
      console.log(`${data}`);
    });
  
    setTimeout(function(){ 
      livereload.reload(); 
    }, 500);
    return;
});

/////////////////////////////////////////////////////////////////////////////////
//                              Utility
/////////////////////////////////////////////////////////////////////////////////

/** generateRandomName 
 *  Produces a 16 random symbol string
 */
var generateRandomName = function() {
    var randomName="";
    for (var i=0; i<16; i++){
      var randomArray=[
        Math.floor((Math.random()*9)+48),
        Math.floor((Math.random()*25)+65),
        Math.floor((Math.random()*25)+97)
      ];
        
      randomName = randomName + String.fromCharCode( randomArray[Math.floor((Math.random()*3))] );
    }
    return randomName;
}

/** deleteFileR
 *  Removes all the files and folders inside the targeted path
 */
var deleteFileR = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) {
        deleteFileR(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};





