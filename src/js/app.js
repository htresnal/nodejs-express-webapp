$( document ).ready(function() {
  var state=0;

  $(".btn").on("click", function(e){
    console.log("CLICK");
    if (state == 0){
      $(".centered").removeClass("well");
      $(".centered").addClass("alert");
      $(".centered").addClass("alert-warning");
      state=1;
    } else {
      $(".centered").addClass("well");
      $(".centered").removeClass("alert");
      $(".centered").removeClass("alert-warning");
      state=0;
    }
  });
  
  $.ajax({
    url: 'data/date.json',
    success: function(resp){
      var obj = JSON.parse(resp);
      $("#date").html(obj.date);
    }
  });
});
